FROM alpine:3.11

RUN apk add --no-cache ca-certificates

ADD openstack-cloud-controller-manager /bin/

CMD ["/bin/openstack-cloud-controller-manager"]
