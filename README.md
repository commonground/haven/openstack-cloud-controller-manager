Workaround external OpenStack Cloud Provider Manager requiring a gophercloud patch, which is included with CCM > v1.17.0 but we are using v1.17.0 with Kops 1.17.

Gophercloud PR: https://github.com/gophercloud/gophercloud/pull/1829
CCM PR: https://github.com/kubernetes/cloud-provider-openstack/pull/924

Upon release of Kops 1.18 using CCM 1.18 this workaround is not needed anymore and should be removed.

Image tagged with 20200211 is based on commit 08824aaabec43b3ede9a8af3e9aed897fa3bf054 (bumping gophercloud).


Build steps:

`git clone https://github.com/kubernetes/cloud-provider-openstack`
`make build-cross TARGETS=linux/amd64`

`cp /path/to/cloud-provider-openstack/_dist/linux-amd64/openstack-cloud-controller-manager .`

`docker build -t registry.gitlab.com/commonground/haven/openstack-cloud-controller-manager:20200211 .`
`docker push registry.gitlab.com/commonground/haven/openstack-cloud-controller-manager:20200211`
